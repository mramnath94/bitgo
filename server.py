import sys
from src.blockchain import Block
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/populate', methods=['POST'])
def populateBlock():
  block.populate_block()
  return jsonify({"status": "Success"})

@app.route('/transactions', methods=['GET'])
def getTransactions():
  offset = request.args.get('offset')
  return jsonify(results=block.fetch_all_transactions(offset))

@app.route('/ancestry', methods=['GET'])
def getAncestry():
  offset = request.args.get('offset')
  return jsonify(results=block.fetch_ancestry_for_transactions(offset))

@app.route('/top-ancestry', methods=['GET'])
def getTopAncestry():
  return jsonify(results=block.fetch_top_ancestry())

if __name__ == '__main__':
  block = Block(680000, "000000000000000000076c036ff5119e5a5a74df77abf64203473364509f7732")
  app.run(port = 5000, debug=True)
  print("server started")