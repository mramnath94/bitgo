import requests
import json
import util.config
from collections import defaultdict
from heapq import heappush, heappop, heapify

class Block:
  def __init__(self, blockId, blockHash):
    self.transactions = []
    self.blockId = blockId
    self.blockHash = blockHash
    self.noOfTransactions = 0
    self.d = defaultdict()

  def _getTransaction(self, id):
    for i in self.transactions:
      if(i.id == id):
        return i
    return {}

  def _getAndValidateOffset(self, offset):
    #validate offset length < nooftransactions
    if not offset:
      offset = 0
    else:
      offset = int(offset)
    if offset > self.noOfTransactions:
      raise Exception("Invalid OffSet")
    return offset

  def _populateAncestryLink(self, transaction, visited):
    if(transaction.id in visited[0]):
      return
    for anc in transaction.ancestry:
      t = self._getTransaction(anc)
      if(t):
        res = self._populateAncestryLink(t, visited)
        if(res):
          transaction.ancestry += res
    visited[0].append(transaction.id)
    return transaction.ancestry


  def fetch_top_ancestry(self):
    heap, res = [], []
    for transaction in self.transactions:
      if(transaction.ancestry):
        # - negative value to construct max heap
        heappush(heap, [-len(transaction.ancestry), transaction.id])
    i = 0   
    print(heap)     
    while(heap and i < util.config.config["top_ancestry_count"]):
      top = heappop(heap)
      if top:
        res.append(top[1])
      i += 1
    return res

  def fetch_ancestry_for_transactions(self, offset):
    offset = self._getAndValidateOffset(offset)
    res = []
    for i in range(offset, offset + util.config.config["offset_increment"] + 1):
      t = []
      t += self.transactions[i].ancestry
      res.append({self.transactions[i].id: t})
    return res


  def fetch_all_transactions(self, offset):
    offset = self._getAndValidateOffset(offset)
    t = list(self.d.keys())
    print(len(t), len(self.transactions))
    return t[offset: offset + util.config.config["offset_increment"] + 1]

  def populate_block(self):
    currOffSet = 0
    getBlockDetailsUrl = util.config.config["base_url"] + self.blockHash
    try:
      blockdata = requests.get(getBlockDetailsUrl)
      blockcount = json.loads(blockdata.text)["tx_count"]
      self.noOfTransactions = blockcount 
    except:
      raise Exception("Error while fetching block data")
    while(currOffSet <= blockcount):
      fetchTransactionsRequestUrl = util.config.config["base_url"] + self.blockHash + "/txs/" + str(currOffSet)
      print("HangTight fetching upto " + str(currOffSet + 25) + " transactions")
      transactions = requests.get(fetchTransactionsRequestUrl)
      if(transactions.status_code != 200):
        if(not self.transactions):
          raise Exception('Transactions are not loaded')
        else:
          break
      for i in json.loads(transactions.text):
        transaction = Transaction()
        transaction.id = i['txid']
        transaction.input = i['vin']
        transaction.output = i['vout']
        transaction.status = i['status']
        if(transaction.id not in self.d):
          self.d[transaction.id] = True
        self.transactions.append(transaction)
      currOffSet += util.config.config["offset_increment"] or 25
    for transaction in self.transactions:
      transaction.populate_ancestry(self.d)
    visited = [[]]
    for transaction in self.transactions:
      if(not transaction.id in visited[0]):
        self._populateAncestryLink(transaction, visited)



class Transaction:
  def __init__(self):
    self.id = ""
    self.input = []
    self.output = []
    self.ancestry = []
    self.status = {}

  def populate_ancestry(self, blockTransactions):
    for i in self.input:
      if(i["is_coinbase"] or i["txid"] not in blockTransactions):
        continue
      self.ancestry.append(i["txid"])


